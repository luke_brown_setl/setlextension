chrome.tabs.onActivated.addListener(function (tabId, changeInfo, tab) {

    updateIcon();
});

chrome.tabs.onUpdated.addListener(function () {
    updateIcon();
});

function updateIcon() {
    chrome.tabs.getSelected(null, function (tab) {

        var tabLink = tab.url;

        if (tabLink.toLocaleLowerCase().indexOf('setl') !== -1) {
            chrome.browserAction.setIcon({
                path: {
                    "48": "images/icon.png",
                    "64": "images/icon.png",
                    "128": "images/icon.png"
                }
            });
        } else {

            chrome.browserAction.setIcon({
                path: {
                    "48": "images/icon-faded.png",
                    "64": "images/icon-faded.png",
                    "128": "images/icon-faded.png"
                }
            });
        }

    });
}